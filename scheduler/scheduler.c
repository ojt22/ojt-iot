
 #include "./scheduler.h"
/*
A function that creates a task structure, the first parameter is the function, the second parameter is the parameters that the function receives, the third parameter is the callback function, the fourth parameter is the number of seconds we want the function to wait until it works again


*/
Task creatTask(void *(*task)(void *), void *param,    void *(*callBack)(void *),int delay_seconds)
{
   Task newTask;

   newTask.task = task;
   newTask.params = param;
   newTask.callBack=callBack;
   newTask.delay_seconds = delay_seconds;
   return newTask;
}

/*
The function receives a structure of a task and runs it together with the parameter defined in the structure
*/
void *schedule_task(void *task)
{
   Task *task_struct = (Task *)task;
   int delay_seconds = task_struct->delay_seconds;
   delay_seconds = delay_seconds > 0 ? delay_seconds : 1;

   while (1)
   {
      task_struct->task(task_struct->params);
      sleep(delay_seconds);
   }
   return NULL;
}
//Meir`s func
int runTask(void *(*task)(void *), void *data, pthread_t *id)
{
   pthread_t curThread;

   if (pthread_create(&curThread, NULL, task, (void *)data) != 0)
   {
      puts("Error creating thread");
      return -1;
   }
   *id = curThread; // to give the function called the ability to stop current thread
   return 0;
}

/*
A function that generates a thread using the structure of the task
*/
void *async_schedule_task(Task *task_struct)
{

   runTask(schedule_task, task_struct,&task_struct->thread);
}

//Meir`s func
void * stop_thread(pthread_t arg){
   return NULL;
}



void *stop_async_schedule_task(Task *arg)
{

   stop_thread(arg->thread);
}




