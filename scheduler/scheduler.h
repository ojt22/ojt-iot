#if !defined(Sceduler)
#define Sceduler
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

//struct
typedef struct Task
{

   int delay_seconds;
   void *(*task)(void *);
   void *(*callBack)(void *);
   void *(*schedule_task)(void *);
   void *params;
   pthread_t thread;
} Task;


Task creatTask(void *(*task)(void *), void *param,    void *(*callBack)(void *),int delay_seconds);
void *schedule_task(void *task);
void *async_schedule_task(Task *task_struct);
void *stop_async_schedule_task(Task *arg);

#endif // Sceduler
